"""Gitlab api interaction tests."""
import json
import unittest
from unittest import mock

from gitlab import Gitlab
from gitlab.v4 import objects as gitlab_objects
import responses

from cki_lib import gitlab
from cki_lib import misc


class TestGitLab(unittest.TestCase):
    """Test GitLab helpers."""

    @staticmethod
    @mock.patch.dict('os.environ', {'GITLAB_TOKENS': '{}'})
    @mock.patch('cki_lib.gitlab.get_token')
    def test_get_instance_get_token(get_token):
        """Check that get_token is used."""
        gitlab.get_instance('https://a')
        get_token.assert_called()

    @staticmethod
    @mock.patch.dict('os.environ', {'GITLAB_TOKENS': '{}'})
    @mock.patch('cki_lib.gitlab.get_token')
    def test_get_instance_no_get_token(get_token):
        """Check that get_token is not used if token is provided."""
        gitlab.get_instance('https://a', token='c')
        get_token.assert_not_called()

    @staticmethod
    @mock.patch.dict('os.environ', {'TEST_TOKENS': '{}'})
    @mock.patch('gitlab.Gitlab')
    def test_get_instance_no_token(mock_gitlab):
        """Check that the provided env name is used."""
        gitlab.get_instance('https://a', env_name='TEST_TOKENS')
        mock_gitlab.assert_called_with(
            'https://a', session=mock.ANY, private_token=None, per_page=gitlab.PER_PAGE)

    @staticmethod
    @mock.patch.dict('os.environ', {'TEST_TOKENS': '{"a":"b"}'})
    @mock.patch('gitlab.Gitlab')
    def test_get_instance_no_token_var(mock_gitlab):
        """Check that the provided env name is used."""
        gitlab.get_instance('https://a', env_name='TEST_TOKENS')
        mock_gitlab.assert_called_with(
            'https://a', session=mock.ANY, private_token=None, per_page=gitlab.PER_PAGE)

    @staticmethod
    @mock.patch.dict('os.environ', {'TEST_TOKENS': '{"a":"b"}', 'b': 'c'})
    @mock.patch('gitlab.Gitlab')
    def test_get_instance_custom_tokens(mock_gitlab):
        """Check that the provided env name is used."""
        gitlab.get_instance('https://a', env_name='TEST_TOKENS')
        mock_gitlab.assert_called_with(
            'https://a', session=mock.ANY, private_token='c', per_page=gitlab.PER_PAGE)

    @staticmethod
    @mock.patch.dict('os.environ', {'GITLAB_TOKENS': '{"a":"d", "a/a": "b"}', 'b': 'c'})
    @mock.patch('gitlab.Gitlab')
    def test_get_instance_longest(mock_gitlab):
        """Check that the longest match is returned."""
        gitlab.get_instance('https://a/a/a')
        mock_gitlab.assert_called_with(
            'https://a', session=mock.ANY, private_token='c', per_page=gitlab.PER_PAGE)


class TestGitLabParseURL(unittest.TestCase):
    """Test parse_gitlab_url."""

    @staticmethod
    def mock_responses():
        """Set up tests."""
        url = 'https://gitlab.com/api/v4/'
        mocks = [
            ('projects/group_foo%2Fproject_foo', {'id': 1}),
            ('projects/group_foo%2Fproject_foo%2Fsubproject_foo', {'id': 2}),
            ('projects/group_foo%2Fproject_foo/pipelines/1234', {'id': 1234}),
            ('projects/group_foo%2Fproject_foo%2Fsubproject_foo/pipelines/1234', {'id': 1234}),
            ('projects/group_foo%2Fproject_foo/jobs/1234', {'id': 1234}),
            ('projects/group_foo%2Fproject_foo%2Fsubproject_foo/jobs/1234', {'id': 1234}),
            ('projects/group_foo%2Fproject_foo/merge_requests/4321', {'id': 4321, 'iid': 12}),
            ('projects/group_foo%2Fproject_foo%2Fsubproject_foo/merge_requests/4321',
             {'id': 4321, 'iid': 12}),
            ('projects/group_foo%2Fproject_foo/merge_requests/12/notes/1122', {'id': 1122}),
            ('projects/group_foo%2Fproject_foo%2Fsubproject_foo/merge_requests/12/notes/1122',
             {'id': 1122}),
            ('projects/group_foo%2Fproject_foo/pipeline_schedules/9', {'id': 1}),
            ('projects/group_foo%2Fproject_foo%2Fsubproject_foo/pipeline_schedules/9', {'id': 2}),
            ('projects/group_foo%2Fproject_foo/issues/1234', {'id': 1234}),
            ('projects/group_foo%2Fproject_foo%2Fsubproject_foo/issues/1234', {'id': 1234}),
            ('groups/group_foo', {'id': 1}),
            ('groups/group_foo%2Fsubgroup_foo', {'id': 2}),
        ]

        for endpoint, payload in mocks:
            responses.add(responses.GET, url + endpoint, json=payload)

    @responses.activate
    def test_get_pipeline(self):
        """Test parse_gitlab_url with pipeline urls."""
        self.mock_responses()
        urls = [
            'https://gitlab.com/group_foo/project_foo/-/pipelines/1234',
            'https://gitlab.com/group_foo/project_foo/subproject_foo/-/pipelines/1234',
        ]
        for url in urls:
            gl_instance, obj = gitlab.parse_gitlab_url(url)

            self.assertIsInstance(gl_instance, Gitlab)
            self.assertIsInstance(obj, gitlab_objects.ProjectPipeline)
            self.assertEqual(1234, obj.id)

    @responses.activate
    def test_get_job(self):
        """Test parse_gitlab_url with job urls."""
        self.mock_responses()
        urls = [
            'https://gitlab.com/group_foo/project_foo/-/jobs/1234',
            'https://gitlab.com/group_foo/project_foo/subproject_foo/-/jobs/1234',
        ]
        for url in urls:
            gl_instance, obj = gitlab.parse_gitlab_url(url)

            self.assertIsInstance(gl_instance, Gitlab)
            self.assertIsInstance(obj, gitlab_objects.ProjectJob)
            self.assertEqual(1234, obj.id)

    @responses.activate
    def test_get_merge_request(self):
        """Test parse_gitlab_url with MR urls."""
        self.mock_responses()
        urls = [
            'https://gitlab.com/group_foo/project_foo/-/merge_requests/4321',
            'https://gitlab.com/group_foo/project_foo/subproject_foo/-/merge_requests/4321',
        ]
        for url in urls:
            gl_instance, obj = gitlab.parse_gitlab_url(url)

            self.assertIsInstance(gl_instance, Gitlab)
            self.assertIsInstance(obj, gitlab_objects.ProjectMergeRequest)
            self.assertEqual(4321, obj.id)

    @responses.activate
    def test_get_merge_request_note(self):
        """Test parse_gitlab_url with MR Note urls."""
        self.mock_responses()
        urls = [
            'https://gitlab.com/group_foo/project_foo/-/merge_requests/4321#note_1122',
            'https://gitlab.com/group_foo/project_foo/subproject_foo/-/merge_requests/4321'
            '#note_1122',
        ]
        for url in urls:
            gl_instance, obj = gitlab.parse_gitlab_url(url)

            self.assertIsInstance(gl_instance, Gitlab)
            self.assertIsInstance(obj, gitlab_objects.ProjectMergeRequestNote)
            self.assertEqual(1122, obj.id)

    @responses.activate
    def test_get_project(self):
        """Test parse_gitlab_url with Project urls."""
        self.mock_responses()
        urls = [
            ('https://gitlab.com/group_foo/project_foo', 1),
            ('https://gitlab.com/group_foo/project_foo/subproject_foo', 2),
        ]
        for url, id in urls:
            gl_instance, obj = gitlab.parse_gitlab_url(url)

            self.assertIsInstance(gl_instance, Gitlab)
            self.assertIsInstance(obj, gitlab_objects.Project)
            self.assertEqual(id, obj.id)

    @responses.activate
    def test_get_schedule(self):
        """Test parse_gitlab_url with pipeline schedule urls."""
        self.mock_responses()
        urls = [
            ('https://gitlab.com/group_foo/project_foo/-/pipeline_schedules/9', 1),
            ('https://gitlab.com/group_foo/project_foo/subproject_foo/-/pipeline_schedules/9', 2),
        ]
        for url, id in urls:
            gl_instance, obj = gitlab.parse_gitlab_url(url)

            self.assertIsInstance(gl_instance, Gitlab)
            self.assertIsInstance(obj, gitlab_objects.ProjectPipelineSchedule)
            self.assertEqual(id, obj.id)

    @responses.activate
    def test_get_issue(self):
        """Test parse_gitlab_url with issue urls."""
        self.mock_responses()
        urls = [
            'https://gitlab.com/group_foo/project_foo/-/issues/1234',
            'https://gitlab.com/group_foo/project_foo/subproject_foo/-/issues/1234',
        ]
        for url in urls:
            gl_instance, obj = gitlab.parse_gitlab_url(url)

            self.assertIsInstance(gl_instance, Gitlab)
            self.assertIsInstance(obj, gitlab_objects.ProjectIssue)
            self.assertEqual(1234, obj.id)

    @responses.activate
    def test_get_group(self):
        """Test parse_gitlab_url with group urls."""
        self.mock_responses()
        urls = (
            ('https://gitlab.com/groups/group_foo', 1),
            ('https://gitlab.com/groups/group_foo/subgroup_foo', 2),
        )
        for url, object_id in urls:
            with self.subTest(url):
                gl_instance, gl_obj = gitlab.parse_gitlab_url(url)
                self.assertIsInstance(gl_instance, Gitlab)
                self.assertIsInstance(gl_obj, gitlab_objects.Group)
                self.assertEqual(gl_obj.id, object_id)


class TestGitLabClient(unittest.TestCase):
    """Test GitLabClient methods."""

    @responses.activate
    @mock.patch.dict('os.environ', {'GITLAB_TOKENS': '{}'})
    def test_query(self) -> None:
        """Test the query method."""
        responses.add(
            responses.POST, 'https://host/api/graphql',
            json={'data': {'currentUser': 'foo'}}
        )
        client = gitlab.get_graphql_client('https://host')
        result = client.query('{currentUser { username }}')
        self.assertIn('currentUser', json.loads(responses.calls[0].request.body)['query'])
        self.assertNotIn('Authorization', responses.calls[0].request.headers)
        self.assertEqual(result, {'currentUser': 'foo'})

    @responses.activate
    @mock.patch.dict('os.environ', {
        'GITLAB_TOKENS': '{"host": "GITLAB_TOKEN"}',
        'GITLAB_TOKEN': 'bar',
    })
    def test_query_auth(self) -> None:
        """Test query authentication."""
        responses.add(
            responses.POST, 'https://host/api/graphql',
            json={'data': {'currentUser': 'foo'}}
        )
        client = gitlab.get_graphql_client('https://host')
        client.query('{currentUser { username }}')
        self.assertEqual(responses.calls[0].request.headers['Authorization'], 'Bearer bar')

    @responses.activate
    def test_query_token(self) -> None:
        """Test query authentication via an explicit token."""
        responses.add(
            responses.POST, 'https://host/api/graphql',
            json={'data': {'currentUser': 'foo'}}
        )
        client = gitlab.get_graphql_client('https://host', token='token')
        client.query('{currentUser { username }}')
        self.assertEqual(responses.calls[0].request.headers['Authorization'], 'Bearer token')

    @responses.activate
    @mock.patch.dict('os.environ', {
        'GITLAB_TOKENS': '{"host": "GITLAB_TOKEN", "host/group": "GITLAB_GROUP_TOKEN"}',
        'GITLAB_GROUP_TOKEN': 'bar',
    })
    def test_query_auth_longest(self) -> None:
        """Test query authentication."""
        responses.add(
            responses.POST, 'https://host/api/graphql',
            json={'data': {'currentUser': 'foo'}}
        )
        client = gitlab.get_graphql_client('https://host/group')
        client.query('{currentUser { username }}')
        self.assertEqual(responses.calls[0].request.headers['Authorization'], 'Bearer bar')
        self.assertEqual(client.url, 'https://host')

    @responses.activate
    def test_query_error(self) -> None:
        """Test query raises the expected error."""
        responses.add(
            responses.POST, 'https://host/api/graphql',
            json={'errors': ['foo']}
        )
        client = gitlab.get_graphql_client('https://host')
        with self.assertRaises(Exception):
            client.query('{currentUser { username }}')

    @responses.activate
    def test_query_paged(self):
        """Test paged query returns the expected results."""
        query = '''
            query($first: Boolean = true, $after: String = "") {
                project(fullPath: "group/project") {
                    id @include(if: $first)
                    mergeRequests(after: $after) {
                        nodes { iid }
                        pageInfo { hasNextPage endCursor }
                    }
                }
            }
        '''
        responses.add(
            responses.POST, 'https://host/api/graphql',
            json={'data': {'project': {'id': 2, 'mergeRequests': {
                'nodes': [{'iid': 1}], 'pageInfo': {'hasNextPage': True, 'endCursor': 'foo'}}}}}
        )
        responses.add(
            responses.POST, 'https://host/api/graphql',
            json={'data': {'project': {'mergeRequests': {
                'nodes': [{'iid': 2}], 'pageInfo': {'hasNextPage': False}}}}}
        )
        client = gitlab.get_graphql_client('https://host')
        result = client.query(query, paged_key='project/mergeRequests')
        self.assertEqual(misc.get_nested_key(
            result, 'project/mergeRequests/nodes'), [{'iid': 1}, {'iid': 2}])

    @responses.activate
    @mock.patch.dict('os.environ', {'GITLAB_TOKENS': '{}'})
    def test_main(self) -> None:
        """Test the main method."""
        responses.add(
            responses.POST, 'https://host/api/graphql',
            json={'data': {'currentUser': 'foo'}}
        )
        result = gitlab.main([
            '--gitlab-url', 'https://host',
            '--graphql-query', '{currentUser { username }}',
            '--variables', 'key=value',
            '--variables', 'foo=bar', 'baz=qux',
        ])
        self.assertIn('currentUser', json.loads(responses.calls[0].request.body)['query'])
        self.assertEqual(json.loads(responses.calls[0].request.body)['variables'],
                         {'key': 'value', 'foo': 'bar', 'baz': 'qux'})
        self.assertEqual(result, json.dumps({'currentUser': 'foo'}))
