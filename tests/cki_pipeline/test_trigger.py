"""Unit tests for cki_pipeline.trigger()"""
import unittest
from unittest import mock

import responses

from cki_lib import cki_pipeline
from cki_lib.gitlab import get_instance

from . import mocks


class TestTrigger(unittest.TestCase, mocks.GitLabMocks):
    """Tests for cki_pipeline.trigger()."""

    variables = {'cki_project': 'cki-project',
                 'cki_pipeline_branch': 'test_branch',
                 'title': 'original-title'}

    def _check_variables(self, is_production=True, empty=False):
        api_requests = self.get_requests(url='https://gitlab.com/api/v4/projects/1/pipeline')
        if empty:
            self.assertFalse(api_requests)
            return None
        request_variables = {v['key']: v['value'] for v in api_requests[0]['variables']}
        if is_production:
            self.assertNotIn('CKI_DEPLOYMENT_ENVIRONMENT', request_variables)
        else:
            self.assertIn('CKI_DEPLOYMENT_ENVIRONMENT', request_variables)
        return request_variables

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline.sleep')
    def test_is_production_true(self, sleep_mock):
        """Check handling of explicit is_production=True."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, self.variables, is_production=True)
        self._check_variables(is_production=True)
        sleep_mock.assert_not_called()

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline.sleep')
    def test_is_production_false(self, sleep_mock):
        """Check handling of explicit is_production=False."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, self.variables, is_production=False)
        self._check_variables(is_production=False)
        sleep_mock.assert_called_with(30)

    @responses.activate
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('cki_lib.cki_pipeline.sleep')
    def test_is_production_none_true(self, sleep_mock):
        """Check handling of implicit is_production=True."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, self.variables, is_production=None)
        self._check_variables(is_production=True)
        sleep_mock.assert_not_called()

    @responses.activate
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    @mock.patch('cki_lib.cki_pipeline.sleep')
    def test_is_production_none_false(self, sleep_mock):
        """Check handling of implicit is_production=False."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, self.variables, is_production=None)
        self._check_variables(is_production=False)
        sleep_mock.assert_called_with(30)

    @responses.activate
    @mock.patch('builtins.input')
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_interactive_false(self, input_mock):
        """Check handling of interactive=False."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, self.variables, is_production=True, interactive=False)
        self._check_variables()
        input_mock.assert_not_called()

    @responses.activate
    @mock.patch('builtins.input')
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_interactive_true_non_prod(self, input_mock):
        """Check handling of interactive=True."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, self.variables, is_production=False, interactive=True)
        self._check_variables(is_production=False)
        input_mock.assert_not_called()

    @responses.activate
    @mock.patch('builtins.input')
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_interactive_true(self, input_mock):
        """Check handling of interactive=True."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        input_mock.return_value = 'YES'
        cki_pipeline.trigger(gl_project, self.variables, is_production=True, interactive=True)
        self._check_variables()
        input_mock.assert_called()

    @responses.activate
    @mock.patch('builtins.input')
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_interactive_true_wrong_input(self, input_mock):
        """Check handling of interactive=True."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        input_mock.return_value = 'not-yes'
        self.assertRaises(Exception, lambda: cki_pipeline.trigger(
            gl_project, self.variables, is_production=True, interactive=True))
        self._check_variables(empty=True)
        input_mock.assert_called()

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_variable_cleaning(self):
        """Check cleaning of variables for is_production=False."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, dict(self.variables, **{
            'test_priority': 'urgent',
            'skip_beaker': 'False',
            'report_rules': '[{"if": "cond", "send_cc": "recipient"}]'
        }), is_production=False)
        variables = self._check_variables(is_production=False)
        self.assertEqual(variables, {
            'cki_project': 'cki-project',
            'cki_pipeline_branch': 'test_branch',
            'title': 'Retrigger: original-title',
            'subject': 'Retrigger: original-title',
            'CKI_DEPLOYMENT_ENVIRONMENT': 'retrigger',
            # 'report_rules': removed
            # 'test_priority': removed
            'skip_beaker': 'true'})

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_variable_cleaning_force(self):
        """Check cleaning of forced variables for is_production=False."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, dict(self.variables, **{
            # 'test_priority': missing
            # 'skip_beaker': missing
        }), is_production=False)
        variables = self._check_variables(is_production=False)
        self.assertEqual(variables, {
            'cki_project': 'cki-project',
            'cki_pipeline_branch': 'test_branch',
            'title': 'Retrigger: original-title',
            'subject': 'Retrigger: original-title',
            'CKI_DEPLOYMENT_ENVIRONMENT': 'retrigger',
            # 'test_priority': removed,
            'skip_beaker': 'true'})

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
    def test_variable_cleaning_override(self):
        """Check that it is possible to override all cleaned variables."""
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki_project')
        self.add_branch(1, 'cki_pipeline_branch')

        gl_project = gl_instance.projects.get(1)

        cki_pipeline.trigger(gl_project, self.variables, variable_overrides={
            'cki_project': 'cki_project',
            'cki_pipeline_branch': 'cki_pipeline_branch',
            'title': 'title',
            'subject': 'subject',
            'CKI_DEPLOYMENT_ENVIRONMENT': 'deployment-env',
            'test_priority': 'priority',
            'skip_beaker': 'skip_beaker',
        }, is_production=False)
        variables = self._check_variables(is_production=False)
        self.assertEqual(variables, {
            'cki_project': 'cki_project',
            'cki_pipeline_branch': 'cki_pipeline_branch',
            'title': 'title',
            'subject': 'subject',
            'CKI_DEPLOYMENT_ENVIRONMENT': 'deployment-env',
            'test_priority': 'priority',
            'skip_beaker': 'skip_beaker'})
