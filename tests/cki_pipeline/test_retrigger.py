"""Unit tests for cki_pipeline.retrigger()"""
import unittest
from unittest import mock

import responses

from cki_lib import cki_pipeline
from cki_lib.gitlab import get_instance

from . import mocks


class TestRetrigger(unittest.TestCase, mocks.GitLabMocks):
    """Tests for cki_pipeline.retrigger()."""

    variables = {'cki_project': 'cki-project',
                 'cki_pipeline_branch': 'test_branch',
                 'title': 'original-title'}

    def _check_variables(self, is_production=True, empty=False):
        api_requests = self.get_requests(url='https://gitlab.com/api/v4/projects/1/pipeline')
        if empty:
            self.assertFalse(api_requests)
            return None
        request_variables = {v['key']: v['value'] for v in api_requests[0]['variables']}
        if is_production:
            self.assertNotIn('CKI_DEPLOYMENT_ENVIRONMENT', request_variables)
        else:
            self.assertIn('CKI_DEPLOYMENT_ENVIRONMENT', request_variables)
        return request_variables

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline._create_custom_configuration',
                mock.Mock(return_value='branch'))
    @mock.patch('cki_lib.cki_pipeline._create_commit', mock.Mock())
    def _check_migrations(self, variables):
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])
        self.add_branch(1, 'branch')
        self.add_gitlab_yml(1)
        gl_project = gl_instance.projects.get(1)
        self.add_pipeline(1, 1, {
            **self.variables,
            **variables,
        })
        cki_pipeline.retrigger(gl_project, 1)
        return self._check_variables(is_production=False)

    def test_variable_migration(self):
        """Check variable migrations."""
        cases = (
            ('package_name missing', {}, {'source_package_name': None}),
            ('package_name present', {'package_name': 'kernel'},
             {'source_package_name': 'kernel'}),
            ('source_package_name present', {'source_package_name': 'foo', 'package_name': 'bar'},
             {'source_package_name': 'foo'}),
            ('nvr present', {'source_package_name': 'kernel', 'nvr': 'kernel-123.el9.src.rpm'},
             {'kernel_version': '123.el9'}),
            ('kernel_version present', {'nvr': 'kernel-1.el9.src.rpm', 'kernel_version': '123.el9'},
             {'kernel_version': '123.el9'}),
            ('AWS_UPT_LAUNCH_TEMPLATE_NAME', {'AWS_UPT_LAUNCH_TEMPLATE_NAME': 'foo'},
             {}),
        )
        for description, before, after in cases:
            with self.subTest(description):
                variables = self._check_migrations(before)
                for key, value in after.items():
                    if value is None:
                        self.assertNotIn(key, variables)
                    else:
                        self.assertEqual(variables[key], value)
