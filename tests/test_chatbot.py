"""Test cki_lib.chatbot module."""
import os
import unittest
from unittest import mock

from cki_lib import chatbot


class TestChatbot(unittest.TestCase):
    """Test cki_lib.chatbot module."""

    @mock.patch.dict(os.environ, {'CHATBOT_EXCHANGE': '', 'CHATBOT_URL': ''})
    def test_send_message(self) -> None:
        """Test cki_lib.chatbot.send_message."""
        cases = (
            ('message queue', {'CHATBOT_EXCHANGE': 'exchange'}, mock.call(
                {'message': 'message'},
                'chatbot.message',
                exchange='exchange',
                headers={'message-type': 'chatbot'},
            ), None),
            ('rest api', {'CHATBOT_URL': 'url'}, None, mock.call(
                'url',
                json={'message': 'message'},
            )),
            ('none', {}, None, None),
        )
        for description, environ, expected_messagequeue, expected_rest in cases:
            with (self.subTest(description),
                  mock.patch.dict(os.environ, environ),
                  mock.patch('cki_lib.messagequeue.MessageQueue') as messagequeue,
                  mock.patch('cki_lib.chatbot.SESSION') as session):
                chatbot.send_message('message')
            if expected_messagequeue:
                messagequeue.return_value.send_message.assert_has_calls([expected_messagequeue])
            else:
                messagequeue.return_value.send_message.assert_not_called()
            if expected_rest:
                session.post.assert_has_calls([expected_rest])
            else:
                session.post.assert_not_called()
