"""Test kcidb/validate.py classes and functions."""
import unittest
from unittest import mock

from cki_lib.kcidb import checks


class TestChecks(unittest.TestCase):
    """Test KCIDB checks."""

    def test_broken_boot_tests(self) -> None:
        """Test broken_boot_tests."""
        cases = (
            ('all error', [
                mock.Mock(id=1, build_id=1, comment='Boot test', status='ERROR'),
                mock.Mock(id=2, build_id=1, comment='Boot test', status='ERROR'),
                mock.Mock(id=3, build_id=2, comment='Boot test', status='PASS'),
                mock.Mock(id=4, build_id=2, comment='Boot test', status='PASS'),
            ], {1, 2}),
            ('all error/miss', [
                mock.Mock(id=1, build_id=1, comment='Boot test', status='ERROR'),
                mock.Mock(id=2, build_id=1, comment='Boot test', status='MISS'),
                mock.Mock(id=3, build_id=2, comment='Boot test', status='PASS'),
                mock.Mock(id=4, build_id=2, comment='Boot test', status='PASS'),
            ], {1, 2}),
            ('one error, one fail', [
                mock.Mock(id=1, build_id=1, comment='Boot test', status='ERROR'),
                mock.Mock(id=2, build_id=1, comment='Boot test', status='FAIL'),
                mock.Mock(id=3, build_id=2, comment='Boot test', status='PASS'),
                mock.Mock(id=4, build_id=2, comment='Boot test', status='PASS'),
            ], set()),
            ('one error, one pass', [
                mock.Mock(id=1, build_id=1, comment='Boot test', status='ERROR'),
                mock.Mock(id=2, build_id=1, comment='Boot test', status='PASS'),
                mock.Mock(id=3, build_id=2, comment='Boot test', status='PASS'),
                mock.Mock(id=4, build_id=2, comment='Boot test', status='PASS'),
            ], set()),
            ('no boot test', [
                mock.Mock(id=1, build_id=1, comment='Other', status='ERROR'),
                mock.Mock(id=2, build_id=1, comment='Test', status='ERROR'),
                mock.Mock(id=3, build_id=2, comment='Boot test', status='PASS'),
                mock.Mock(id=4, build_id=2, comment='Boot test', status='PASS'),
            ], set()),
            ('multiple', [
                mock.Mock(id=1, build_id=1, comment='Boot test', status='ERROR'),
                mock.Mock(id=2, build_id=1, comment='Boot test', status='ERROR'),
                mock.Mock(id=3, build_id=2, comment='Boot test', status='MISS'),
                mock.Mock(id=4, build_id=2, comment='Boot test', status='MISS'),
            ], {1, 2, 3, 4}),
        )

        for description, data, expeced_test_ids in cases:
            with self.subTest(description):
                self.assertEqual({t.id for t in checks.broken_boot_tests(data)}, expeced_test_ids)
