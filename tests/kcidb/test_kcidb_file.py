"""Test kcidb/file.py"""
import contextlib
import json
from pathlib import Path
import tempfile
import unittest
from unittest import mock

from cki_lib.kcidb import MAJOR_VERSION
from cki_lib.kcidb import MINOR_VERSION
from cki_lib.kcidb import ValidationError
from cki_lib.kcidb.file import KCIDBFile
from cki_lib.kcidb.file import ObjectNotFound


class TestKCIDBFile(unittest.TestCase):
    """Test KCIDBFile."""

    invalid_content = {
        # missing version would cause special validation errors we are not interested in
        'version': {'major': MAJOR_VERSION, 'minor': MINOR_VERSION},
        'bad_key': 'value',
    }
    checkout1 = {
        'id': 'redhat:checkout1',
        'origin': 'redhat'
    }
    checkout2 = {
        'id': 'redhat:checkout2',
        'origin': 'redhat'
    }
    checkout3 = {
        'id': 'redhat:checkout3',
        'origin': 'redhat'
    }
    build1 = {
        'id': 'redhat:build1',
        'checkout_id': 'redhat:checkout1',
        'origin': 'redhat'
    }
    build2 = {
        'id': 'redhat:build2',
        'checkout_id': 'redhat:checkout2',
        'origin': 'redhat'
    }
    build3 = {
        'id': 'redhat:build3',
        'checkout_id': 'redhat:checkout3',
        'origin': 'redhat',
    }
    test1 = {
        'id': 'redhat:test1',
        'build_id': 'redhat:build1',
        'origin': 'redhat',
        'status': 'PASS',
    }
    test2 = {
        'id': 'redhat:test2',
        'build_id': 'redhat:build2',
        'origin': 'redhat',
        'status': 'PASS',
    }
    valid_content = {
        'version': {'major': MAJOR_VERSION, 'minor': MINOR_VERSION},
        'tests': [],
    }

    @classmethod
    @contextlib.contextmanager
    def dump_kcidb_json(cls, kcidb_data):
        """Context manager that writes the given data into a file and removes it on exit."""
        tmpdir = tempfile.TemporaryDirectory()
        path = Path(tmpdir.name, 'kcidb_data.json')
        path.write_text(json.dumps(kcidb_data), encoding='utf-8')
        try:
            yield path
        finally:
            tmpdir.cleanup()

    @classmethod
    @contextlib.contextmanager
    def create_kcidb_file(cls, kcidb_data):
        """Context manager that writes the given data using KCIDBFile and removes it on exit."""
        with cls.dump_kcidb_json(kcidb_data) as path:
            kcidb_file = KCIDBFile(str(path))
            try:
                yield (path, kcidb_file)
            finally:
                pass

    def test_init_raises(self):
        """__init__ raises for an existing but empty file."""
        with tempfile.NamedTemporaryFile('w+') as file:
            with self.assertRaises(json.decoder.JSONDecodeError):
                KCIDBFile(file.name)

    def test_init_no_save(self):
        """__init__ does not create a file."""
        with tempfile.TemporaryDirectory() as tmpdir:
            path = Path(tmpdir, 'kcidb_data.json')
            KCIDBFile(str(path))
            self.assertFalse(path.exists())

    def test_init_save(self):
        """__init__ + save creates a file with KCIDBFile.DEFAULT_CONTENT."""
        with tempfile.TemporaryDirectory() as tmpdir:
            path = Path(tmpdir, 'kcidb_data.json')
            KCIDBFile(str(path)).save()
            new_content = json.loads(path.read_text(encoding='utf-8'))
            self.assertEqual(new_content, KCIDBFile.DEFAULT_CONTENT)

    @unittest.skipIf(MINOR_VERSION == 0, "Can't test upgrade when minor version is 0")
    def test_saving_upgrades_schema_minor_version(self):
        """Opening a file with an older schema version upgrades it."""
        older = {'version': {'major': MAJOR_VERSION, 'minor': MINOR_VERSION-1}}
        current = {'version': {'major': MAJOR_VERSION, 'minor': MINOR_VERSION}}
        with self.create_kcidb_file(older) as (path, kcidb_file):
            kcidb_file.save()
            with path.open() as updated_kcidb_file:
                new_content = json.load(updated_kcidb_file)
            self.assertEqual(current, new_content)

    def test_no_modifications(self):
        """Opening and saving a file does not make any modifications."""
        with self.create_kcidb_file(self.valid_content) as (path, kcidb_file):
            kcidb_file.save()
            with path.open() as file:
                new_content = json.load(file)
            self.assertEqual(self.valid_content, new_content)

    def test_validate(self):
        """__init__ raises when validate is default and file has invalid content."""
        with self.assertRaises(ValidationError):
            with self.create_kcidb_file(self.invalid_content):
                pass

    def test_no_validate(self):
        """__init__ does not call validate when validate == False."""
        with self.dump_kcidb_json(self.invalid_content) as path:
            with mock.patch('cki_lib.kcidb.file.validate_extended_kcidb_schema') as mock_validate:
                KCIDBFile(str(path), validate=False)
                self.assertFalse(mock_validate.called)

    def test_checkout_and_build(self):
        """
        checkout and build:
        1. return empty dict when file has no checkouts or builds or empty checkouts and builds
        2. return first checkout or build when it exists
        3. allow modifying that dict
        4. allow replacing that dict
        """
        for attr in ('checkout', 'build'):
            cases = (
                (self.valid_content, {}),
                (
                    {**self.valid_content, f'{attr}s': []},
                    {}
                ),
                (
                    {**self.valid_content, f'{attr}s': [getattr(self, f'{attr}1')]},
                    getattr(self, f'{attr}1')
                ),
            )
            for kcidb_data, expected_object in cases:
                with self.subTest(attr=attr, kcidb_data=kcidb_data,
                                  expected_object=expected_object):
                    with self.create_kcidb_file(kcidb_data) as (path, kcidb_file):
                        self.assertEqual(expected_object, getattr(kcidb_file, attr))

                        attr2 = getattr(self, f'{attr}2')
                        getattr(kcidb_file, attr).update(attr2)
                        kcidb_file.save()
                        kcidb_file2 = KCIDBFile(str(path))
                        self.assertEqual(getattr(kcidb_file2, attr), attr2)

                        attr3 = getattr(self, f'{attr}3')
                        setattr(kcidb_file2, attr, attr3)
                        kcidb_file2.save()
                        kcidb_file3 = KCIDBFile(str(path))
                        self.assertEqual(getattr(kcidb_file3, attr), attr3)

    def test_multiple_objects(self):
        """checkout and build raise when there are multiple objects in checkouts and builds."""
        cases = (
            (
                'checkout',
                {**self.valid_content, 'checkouts': [self.checkout1, self.checkout2]},
            ),
            (
                'build',
                {**self.valid_content, 'builds': [self.build1, self.build2]},
            ),
        )
        for attr, kcidb_data in cases:
            with self.subTest(attr=attr, kcidb_data=kcidb_data):
                with self.create_kcidb_file(kcidb_data) as (_, kcidb_file):
                    with self.assertRaises(LookupError):
                        getattr(kcidb_file, attr)
                    with self.assertRaises(LookupError):
                        setattr(kcidb_file, attr, f'{attr}1')

    def test_get_obj(self):
        # pylint: disable=protected-access
        """Get object by id."""
        kcidb_data = {
            **self.valid_content,
            'checkouts': [self.checkout1, self.checkout2],
            'builds': [self.build1, self.build2],
        }

        with self.create_kcidb_file(kcidb_data) as (_, kcidb_file):
            self.assertEqual(
                kcidb_file._get_obj('checkouts', self.checkout1['id']),
                self.checkout1
            )
            self.assertEqual(
                kcidb_file._get_obj('builds', self.build2['id']),
                self.build2
            )

    def test_set_obj(self):
        # pylint: disable=protected-access
        """Set object by id."""
        kcidb_data = {
            **self.valid_content,
        }

        with self.create_kcidb_file(kcidb_data) as (_, kcidb_file):
            kcidb_file._set_obj('checkouts', 'not:present-id', {'id': 'not:present-id'})
            self.assertEqual(
                kcidb_file.data,
                {**kcidb_data, 'checkouts': [{'id': 'not:present-id'}]}
            )

    def test_set_obj_replace(self):
        # pylint: disable=protected-access
        """
        Set object by id.

        Ensure set_obj replaces the object if it's already there.
        """
        kcidb_data = {
            **self.valid_content,
            'checkouts': [self.checkout1, self.checkout2],
            'builds': [self.build1, self.build2],
        }

        with self.create_kcidb_file(kcidb_data) as (_, kcidb_file):
            kcidb_file._set_obj('checkouts', self.checkout1['id'], {'foo': 'bar'})
            self.assertEqual(
                kcidb_file.data,
                {**kcidb_data, 'checkouts': [{'foo': 'bar'}, self.checkout2]}
            )

            kcidb_file._set_obj('builds', self.build2['id'], {'foo': 'bar'})
            self.assertEqual(
                kcidb_file.data,
                {
                    **kcidb_data,
                    'checkouts': [{'foo': 'bar'}, self.checkout2],
                    'builds': [self.build1, {'foo': 'bar'}]
                }
            )

    def test_get_checkout(self):
        """Get checkout by id."""
        kcidb_data = {
            **self.valid_content,
            'checkouts': [self.checkout1, self.checkout2],
            'builds': [self.build1, self.build2],
        }

        with self.create_kcidb_file(kcidb_data) as (_, kcidb_file):
            self.assertEqual(
                kcidb_file.get_checkout(self.checkout1['id']),
                self.checkout1
            )

            self.assertEqual(
                kcidb_file.get_checkout(self.checkout2['id']),
                self.checkout2
            )

            self.assertRaises(ObjectNotFound, kcidb_file.get_checkout, 'missing:id')

    def test_get_build(self):
        """Get build by id."""
        kcidb_data = {
            **self.valid_content,
            'checkouts': [self.checkout1, self.checkout2],
            'builds': [self.build1, self.build2],
        }

        with self.create_kcidb_file(kcidb_data) as (_, kcidb_file):
            self.assertEqual(
                kcidb_file.get_build(self.build1['id']),
                self.build1
            )

            self.assertEqual(
                kcidb_file.get_build(self.build2['id']),
                self.build2
            )

            self.assertRaises(ObjectNotFound, kcidb_file.get_build, 'missing:id')

    def test_set_checkout(self):
        """Set checkout by id."""
        kcidb_data = {
            **self.valid_content,
        }

        with self.create_kcidb_file(kcidb_data) as (_, kcidb_file):
            self.assertRaises(ObjectNotFound, kcidb_file.get_checkout, self.checkout1['id'])

            kcidb_file.set_checkout(self.checkout1['id'], self.checkout1)
            self.assertEqual(
                kcidb_file.data,
                {**kcidb_data, 'checkouts': [self.checkout1]},
            )

            kcidb_file.set_checkout(self.checkout2['id'], self.checkout2)
            self.assertEqual(
                kcidb_file.data,
                {**kcidb_data, 'checkouts': [self.checkout1, self.checkout2]},
            )

    def test_set_build(self):
        """Set build by id."""
        kcidb_data = {
            **self.valid_content,
        }

        with self.create_kcidb_file(kcidb_data) as (_, kcidb_file):
            self.assertRaises(ObjectNotFound, kcidb_file.get_build, self.build1['id'])

            kcidb_file.set_build(self.build1['id'], self.build1)
            self.assertEqual(
                kcidb_file.data,
                {**kcidb_data, 'builds': [self.build1]},
            )

            kcidb_file.set_build(self.build2['id'], self.build2)
            self.assertEqual(
                kcidb_file.data,
                {**kcidb_data, 'builds': [self.build1, self.build2]},
            )

    def test_get_test(self):
        """Get test by id."""
        kcidb_data = {
            **self.valid_content,
            'checkouts': [self.checkout1, self.checkout2],
            'builds': [self.build1, self.build2],
            'tests': [self.test1, self.test2],
        }

        with self.create_kcidb_file(kcidb_data) as (_, kcidb_file):
            self.assertEqual(
                kcidb_file.get_test(self.test1['id']),
                self.test1
            )

            self.assertEqual(
                kcidb_file.get_test(self.test2['id']),
                self.test2
            )

            self.assertRaises(ObjectNotFound, kcidb_file.get_test, 'missing:id')

    def test_set_test(self):
        """Set test by id."""
        kcidb_data = {
            **self.valid_content,
        }

        with self.create_kcidb_file(kcidb_data) as (_, kcidb_file):
            self.assertRaises(ObjectNotFound, kcidb_file.get_test, self.test1['id'])

            kcidb_file.set_test(self.test1['id'], self.test1)
            self.assertEqual(
                kcidb_file.data,
                {**kcidb_data, 'tests': [self.test1]},
            )

            kcidb_file.set_test(self.test2['id'], self.test2)
            self.assertEqual(
                kcidb_file.data,
                {**kcidb_data, 'tests': [self.test1, self.test2]},
            )
