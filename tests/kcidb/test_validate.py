"""Test kcidb/validate.py classes and functions."""
import json
import pathlib
import tempfile
import unittest
from unittest import mock

import jsonschema

from cki_lib import misc
from cki_lib import yaml
from cki_lib.kcidb import MAJOR_VERSION
from cki_lib.kcidb import MINOR_VERSION
from cki_lib.kcidb import ValidationError
from cki_lib.kcidb import sanitize_all_kcidb_status
from cki_lib.kcidb import sanitize_kcidb_status
from cki_lib.kcidb import validate
from cki_lib.kcidb import validate_extended_kcidb_schema

KCIDB_SCHEMA_VERSION = {'major': MAJOR_VERSION, 'minor': MINOR_VERSION}

PATH_TO_KCIDB_ALL = "tests/kcidb/fixtures/kcidb_all.yml"

PROVENANCE_DATA = [
    {'function': 'coordinator', 'url': 'https://coordinator'},
    {'function': 'provisioner', 'url': 'https://provisioner'},
    {'function': 'executor', 'url': 'https://executor', 'service_name': 'gitlab'},
]
CHECKOUT_DATA = {
    'id': 'redhat:1234',
    'origin': 'redhat',
    'misc': {
        'retrigger': False,
        'scratch': False,
        'provenance': PROVENANCE_DATA,
        'actions': {
            'last_triaged_at': '2021-09-01T12:44:41.661629Z',
        },
        'patchset_modified_files': [
            {'path': 'file_a'},
            {'path': 'file_b'},
        ],
        'affected_subsystems': [
            {'name': 'MEMORY MANAGEMENT', 'label': 'mm'},
        ],
        'brew_task_id': 123,
        'source_package_name': 'kernel',
        'submitter': 'Someone <someone@mail.com>',
        'mr': {
            'id': 1,
            'url': 'https://mr.url',
            'diff_url': 'https://mr.url/diff'
        },
        'public': True,
        'kernel_version': '1.2.3.4',
        'all_sources_targeted': True,
        'report_rules': json.dumps([
            {'when': 'condition', 'send_to': 'some_group', 'send_bcc': 'some_other_group'},
            {'when': 'condition', 'send_to': 'some_group', 'report_template': 'long'},
            {'when': 'condition', 'send_bcc': 'some_other_group', 'report_template': 'short'},
            {'when': 'condition',
             'send_to': ['recipient_a', 'recipient_b'],
             'send_bcc': ['recipient_a', 'recipient_b']},
        ])
    }
}
BUILD_DATA = {
    'id': 'redhat:1234_build',
    'checkout_id': 'redhat:1234',
    'origin': 'redhat',
    'misc': {
        'provenance': PROVENANCE_DATA,
        'debug': False,
        'actions': {
            'last_triaged_at': '2021-09-01T12:44:41.661629Z',
        },
        'test_plan_missing': False,
        'package_name': 'kernel',
        'testing_skipped_reason': 'unsupported',
        'kpet_tree_name': 'name',
    }
}
TEST_DATA = {
    'id': 'redhat:1234_test',
    'build_id': 'redhat:1234_build',
    'origin': 'redhat',
    'status': 'PASS',
    'misc': {
        'provenance': PROVENANCE_DATA,
        'beaker': {
            'finish_time': '2021-09-01T12:44:41.661629Z',
            'recipe_id': 123,
            'retcode': 0,
            'task_id': 234,
        },
        'debug': True,
        'fetch_url': 'http://fetch.url',
        'targeted': True,
        'rerun_index': 1,
        'polarion_id': '2111a0fa-1c27-4663-94d5-a951c509413e',
        'maintainers': [
            {'name': 'Maint 1', 'email': 'maint_1@mail.com'},
            {'name': 'Maint 1', 'email': 'maint_1@mail.com', 'gitlab': 'maint_1'},
        ],
        'actions': {
            'last_triaged_at': '2021-09-01T12:44:41.661629Z',
        },
        'results': [
            {'id': 'r:1', 'comment': '/test 1 2 3', 'status': 'PASS'},
            {'id': 'r:2', 'comment': '/test 4 5 6', 'status': 'FAIL',
             'output_files': [{'name': 'log', 'url': 'http://file'}]},
        ],
        'forced_skip_status': True,
    }
}
VALID_DATA = {
    'version': KCIDB_SCHEMA_VERSION,
    'checkouts': [
        CHECKOUT_DATA,
    ],
    'builds': [
        BUILD_DATA,
    ],
    'tests': [
        TEST_DATA,
    ]
}


class TestSchema(unittest.TestCase):
    """Test schema validation."""

    def test_valid_schema(self):
        """Test schema is valid."""
        validate_extended_kcidb_schema(VALID_DATA)

    def test_validate_real_example(self):
        """Assert that the CKI's extended KCIDB schema works as expected."""
        data = yaml.load(file_path=PATH_TO_KCIDB_ALL)

        # Should not raise
        validate_extended_kcidb_schema(data)

    def test_valid_complete_schema(self):
        """Test schema is valid."""
        validate_extended_kcidb_schema(
            {'version': KCIDB_SCHEMA_VERSION}
        )

        validate_extended_kcidb_schema(
            {'version': KCIDB_SCHEMA_VERSION,
             'checkouts': [CHECKOUT_DATA]}
        )

        validate_extended_kcidb_schema(
            {'version': KCIDB_SCHEMA_VERSION,
             'builds': [BUILD_DATA]}
        )

        validate_extended_kcidb_schema(
            {'version': KCIDB_SCHEMA_VERSION,
             'tests': [TEST_DATA]}
        )

    def test_invalid(self):
        """Test invalid schema."""
        cki_error_expected_pattern = (
            r"Failed to validate against the CKI's extended KCIDB schema.")

        cases = [
            ("Wrong type property", {"scratch": "false"}),
        ]

        for description, invalid_props in cases:
            with self.subTest(description):
                self.assertRaisesRegex(
                    ValidationError,
                    cki_error_expected_pattern,
                    validate_extended_kcidb_schema,
                    {
                        'version': KCIDB_SCHEMA_VERSION,
                        'checkouts': [{
                            **CHECKOUT_DATA,
                            "misc": {**CHECKOUT_DATA["misc"], **invalid_props},
                        }]
                    }
                )

    def test_invalid_upstream(self):
        """Test invalid upstream schema."""
        self.assertRaises(
            ValidationError,
            validate_extended_kcidb_schema,
            {'version': KCIDB_SCHEMA_VERSION,
             'checkouts': [{}]}
        )


class TestValidate(unittest.TestCase):
    """Test validation methods."""

    def setUp(self):
        """Set up json file."""
        self.file_path = tempfile.NamedTemporaryFile()
        self.file = pathlib.Path(self.file_path.name)
        self.file.write_text(json.dumps(VALID_DATA), encoding="utf-8")

    @mock.patch('cki_lib.kcidb.validate.validate_extended_kcidb_schema')
    def test_cli(self, mock_validate):
        """Test cli call."""
        validate.main([self.file_path.name])
        mock_validate.assert_called_with(VALID_DATA)

    @mock.patch('cki_lib.kcidb.validate.jsonschema.validate')
    @mock.patch('kcidb_io.schema.V4.validate')
    def test_validate_extended_kcidb_schema(self, mock_kcidb_validate, mock_validate):
        """Test validate calls both upstream and cki validate."""
        validate_extended_kcidb_schema(VALID_DATA)
        mock_kcidb_validate.assert_called_with(VALID_DATA)
        mock_validate.assert_called_with(
            instance=VALID_DATA,
            schema=validate.SCHEMA,
            format_checker=jsonschema.Draft7Validator.FORMAT_CHECKER
        )

    @mock.patch('cki_lib.kcidb.validate.validate_extended_kcidb_schema')
    def test_validate(self, mocked_validate):
        """Assert deprecated validate function warns about deprecation and still works."""
        with self.assertWarns(DeprecationWarning):
            validate.validate(mock.sentinel.input)

        mocked_validate.assert_called_once_with(mock.sentinel.input, raise_for_cki=False)

    @mock.patch('cki_lib.kcidb.validate._validate_kcidb')
    @mock.patch('cki_lib.kcidb.validate._validate_checkouts_origin')
    def test_validate_kcidb(self, mocked_validate_origin, mocked_validate_kcidb):
        """Assert deprecated validate_kcidb function warns about deprecation and still works."""
        with self.assertWarns(DeprecationWarning):
            validate.validate_kcidb(mock.sentinel.input)

        mocked_validate_origin.assert_called_once_with(mock.sentinel.input)
        mocked_validate_kcidb.assert_called_once_with(mock.sentinel.input)

    def test_sanitize_kcidb_status(self):
        """Check sanitize_kcidb_status function."""
        cases = (
            ('Sending Fail', 'Fail', 'FAIL'),
            ('Sending Error', 'Error', 'ERROR'),
            ('Sending Pass', 'Pass', 'PASS'),
            ('Sending Done', 'Done', 'DONE'),
            ('Sending Skip', 'Skip', 'SKIP'),
            ('Sending Warn', 'Warn', 'ERROR'),
            ('Sending Warn/aborted', 'WARN/ABORTED', 'ERROR'),
            ('Sending Panic', 'Panic', 'FAIL'),
            ('Sending unexpected value', '77', 'ERROR'),
        )
        for description, result, status in cases:
            with self.subTest(description):
                self.assertEqual(status, sanitize_kcidb_status(result))

    def test_sanitize_all_kcidb_status(self):
        """Check sanitize_all_kcidb_status function."""
        cases = (
            ('WARN -> ERROR',
             'tests/1/status', 'WARN',
             'tests/1/status', 'ERROR'),
            ('WARN/ABORTED -> ERROR',
             'tests/2/status', 'WARN/ABORTED',
             'tests/2/status', 'ERROR'),
            ('Panic -> FAIL',
             'tests/2/misc/results/0/status', 'Panic',
             'tests/2/misc/results/0/status', 'FAIL'),
            ('worse subtest results are used',
             'tests/2/misc/results/0/status', 'FAIL',
             'tests/2/status', 'FAIL'),
            ('better subtest results are ignored',
             'tests/4/misc/results/2/status', 'PASS',
             'tests/4/status', 'FAIL'),
        )

        for description, path, result, expected_path, expected_result in cases:
            with self.subTest(description):
                data = yaml.load(file_path=PATH_TO_KCIDB_ALL)
                misc.set_nested_key(data, path, result)
                sanitize_all_kcidb_status(data)
                self.assertEqual(misc.get_nested_key(data, expected_path), expected_result)

    def test_sanitize_all_kcidb_status_alerts_inconsistencies(self):
        """Check sanitize_all_kcidb_status function."""
        data = {
            'version': KCIDB_SCHEMA_VERSION,
            'tests': [{
                'id': 'redhat:1234_test',
                'build_id': 'redhat:1234_build',
                'origin': 'redhat',
                'status': 'PASS',
                'misc': {
                    'results': [
                        {'id': 'r:1', 'comment': '/test 1 2 3', 'status': 'PASS'},
                        {'id': 'r:2', 'comment': '/test 4 5 6', 'status': 'FAIL'}
                    ],
                },
            }],
        }

        with self.subTest(test="PASS", results="FAIL"):
            with self.assertLogs(validate.LOGGER, level="ERROR") as log_ctx:
                sanitize_all_kcidb_status(data)

            expected_log = (
                f"ERROR:{validate.LOGGER.name}:"
                "Mismatching Test.status and greatest TestResult.status ('PASS' != 'FAIL')"
            )
            self.assertIn(expected_log, log_ctx.output)

        with self.subTest(test=None, results="FAIL"):
            del data['tests'][0]['status']

            with self.assertLogs(validate.LOGGER, level="ERROR") as log_ctx:
                sanitize_all_kcidb_status(data)

            expected_log = (
                f"ERROR:{validate.LOGGER.name}:"
                "Mismatching Test.status and greatest TestResult.status (None != 'FAIL')"
            )
            self.assertIn(expected_log, log_ctx.output)

    def test_validate_checkouts_origin(self):
        """Test origin validation."""
        test_data = (
            ('Valid data',
             [{'id': 'foo:1', 'origin': 'foo'}, {'id': 'bar:1', 'origin': 'bar'}], True),
            ('Missing origin',
             [{'id': 'foo:1', 'origin': 'foo'}, {'id': 'bar:1'}], False),
            ('Invalid origin',
             [{'id': 'foo:1', 'origin': 'bar'}, {'id': 'bar:1', 'origin': 'foo'}], False),
        )

        for description, checkouts, valid in test_data:
            with self.subTest(description):
                data = {'version': KCIDB_SCHEMA_VERSION, 'checkouts': checkouts}
                if valid:
                    # pylint: disable=protected-access
                    validate._validate_checkouts_origin(data)
                else:
                    self.assertRaises(ValidationError, validate_extended_kcidb_schema, data)
