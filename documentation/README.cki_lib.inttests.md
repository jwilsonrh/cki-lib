---
title: cki_lib.inttests
linkTitle: inttests
description: Run integration tests via kind
---

Integration tests that require other services can be run via [kind][kind]
(Kubernetes in Docker).

Currently, a RabbitMQ service including certificates is implemented.

## Creating an integration test

For an integration test, create a unit test in `/inttests`, and derive the TestCase from
`cki_lib.inttests.KubernetesIntegrationTest`. As an example, for an integration
test that needs a RabbitMQ server:

```python
from cki_lib import inttests

@inttests.skip_without_requirements()
class TestStompClient(inttests.KubernetesIntegrationTest,
                      inttests.rabbitmq.RabbitMQServer):
    pass
```

During `setupClass()`, a Kubernetes cluster and any required services will be
created and configured. The services will be removed in `tearDownClass()`, but
the cluster will be kept running to speed up successive tests.

## API

### KubernetesCluster - Kubernetes cluster running in Docker

- test case class: `inttests.cluster.KubernetesCluster`

The cluster is able to run locally and in GitLab CI as long as the `kind` and
`docker` executables are available. This can be checked with the
`@skip_without_requirements` decorator. For GitLab CI, jobs need to have the
Docker-in-Docker service configured in `.gitlab-ci.yml`:

```yaml
integration-tests:
  services:
    - docker:dind
  variables:
    DOCKER_HOST: 'tcp://docker:2375'
```

#### Fields provided by KubernetesCluster

- `cls.cluster_name: str`: `kind` cluster name
- `cls.hostname: str`: host name for all services usable locally and in GitLab CI
- `cls.api_client: client.ApiClient`: Python `kubernetes` API client
- `cls.dynamic_client: dynamic.client.DynamicClient`: Python `kubernetes` Dynamic client
- `cls.core_v1: client.CoreV1Api`: Python `kubernetes` Core v1 API

#### Methods provided by KubernetesCluster

- `cls.k8s_namespace(namespace: str)`: context manager that creates and
    cleans up a Kubernetes namespace.
- `cls.k8s_apply(namespace: str, body: typing.Any)`: apply a Kubernetes
  resource update via server-side apply.
- `cls.k8s_delete_all(namespace: str, resources: Iterable[str])`: delete all
  resources of the given types from a namespace.

### KubernetesIntegrationTest - Kubernetes namespace

- test case class: `inttests.cluster.KubernetesIntegrationTest`

A dedicated test namespace is created and cleaned up for each test.

#### Fields provided by KubernetesIntegrationTest

- `cls.test_namespace: str`: namespace for any custom resources needed by
  an integration test

### RabbitMQServer - RabbitMQ server

- test case class: `inttests.rabbitmq.RabbitMQServer`

The server is configured with AMQP and STOMP access with and without TLS. For
AMQP and STOMP, authentication can be done via password and client certificates
(from `cert.common_name`) methods.

#### Ports provided by RabbitMQServer

| Port  | Description                 |
|-------|-----------------------------|
| 5671  | AMQP via TLS                |
| 5672  | AMQP                        |
| 15671 | HTTP management API via TLS |
| 15672 | HTTP management API         |
| 61613 | Stomp                       |
| 61614 | Stomp via TLS               |

#### Test environment variables provided by RabbitMQServer

| Name                       | Description                |
|----------------------------|----------------------------|
| `RABBITMQ_HOST`            | host name                  |
| `RABBITMQ_PORT`            | `5671`                     |
| `RABBITMQ_MANAGEMENT_PORT` | `15671`                    |
| `RABBITMQ_VIRTUAL_HOST`    | `/`                        |
| `RABBITMQ_USER`            | `guest`                    |
| `RABBITMQ_PASSWORD`        | `guest`                    |
| `RABBITMQ_CAFILE`          | CA certificate             |
| `RABBITMQ_CERTFILE`        | client key and certificate |
| `STOMP_HOST`               | host name                  |
| `STOMP_PORT`               | `61614`                    |
| `STOMP_CERTFILE`           | client key and certificate |

[Kind]: https://kind.sigs.k8s.io/
