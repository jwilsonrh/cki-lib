"""Helpers operating on datawarehouse.objects."""

import typing


def broken_boot_tests(tests: list[typing.Any]) -> set[typing.Any]:
    """Return a set of broken boot tests from builds where _all_ of them failed.

    Arguments:
      tests: list[datawarehouse.objects.KCIDBTest]: tests to evaluate

    Return value:
      set[datawarehouse.objects.KCIDBTest]: non-FAIL/PASS boot tests of affected builds
    """
    build_ids = {t.build_id for t in tests}
    result = set()
    for build_id in build_ids:
        boot_tests = {t for t in tests
                      if t.build_id == build_id and t.comment == 'Boot test'}
        if boot_tests and not any(t.status in {'FAIL', 'PASS'} for t in boot_tests):
            result |= boot_tests
    return result
