"""CKI's extended KCIDB schema validation.

Provides `validate_kcidb(data)` to validate a dict against the CKI's schema,
which raises a `ValidationError` in case of problems.
"""
import argparse
import json
import pathlib
import pkgutil
from warnings import warn

import jsonschema
from kcidb_io.schema import LATEST as KCIDB_SCHEMA
import yaml

from cki_lib import misc
from cki_lib.logger import get_logger

LOGGER = get_logger(__name__)

SCHEMA = yaml.safe_load(pkgutil.get_data(__name__, 'schema.yaml'))['schema']

KCIDB_STATUS_CHOICES = misc.get_nested_key(
    KCIDB_SCHEMA.json, '$defs/status/enum',
    default=["FAIL", "ERROR", "MISS", "PASS", "DONE", "SKIP"])


class ValidationError(jsonschema.exceptions.ValidationError):
    """The data was invalid under the CKI's extended KCIDB JSON schema."""

    # The main goal here is having an abstraction over the error raised by
    # the third-party implementation of the JSON schema validation,
    # while making the most out of its __str__() method,
    # which currently provides a lot of detail.

    def __init__(
        self,
        message: str,
        validation_error: jsonschema.exceptions.ValidationError = None,
    ):
        """Initialize error instance.

        Args:
            message: Short explanation of what went wrong.
            validation_error: Optional instance of the error raised by the
                3rd-party JSON schema validation with additional information
        """
        validation_error_kwargs = {}
        if validation_error:
            message = message + " " + validation_error.message
            validation_error_kwargs = {
                "validator": validation_error.validator,
                "path": validation_error.path,
                "cause": validation_error.cause,
                "context": validation_error.context,
                "validator_value": validation_error.validator_value,
                "instance": validation_error.instance,
                "schema": validation_error.schema,
                "schema_path": validation_error.schema_path,
                "parent": validation_error.parent,
                "type_checker": validation_error._type_checker,
            }

        super().__init__(message, **validation_error_kwargs)


def sanitize_kcidb_status(result: str) -> str:
    """Sanitize the result from tests and subtests.

    Beaker/Restraint/subtests have some "interesting" task states that are not
    available in KCIDB. Sanitize the known ones, and fall back to ERROR otherwise.
    """
    status = result.upper()
    if status in {'WARN', 'WARN/ABORTED'}:
        return "ERROR"
    if status == 'PANIC':
        return "FAIL"
    if status not in KCIDB_STATUS_CHOICES:
        LOGGER.warning("Parsing invalid status %r into ERROR", status)
        return "ERROR"

    return status


def sanitize_all_kcidb_status(data) -> None:
    """Sanitize the whole payload, converting known invalid values into valid.

    NOTE: The operation is done inplace.
    """
    for test in data.get("tests", []):
        if status := test.get("status"):
            test["status"] = sanitize_kcidb_status(status)

        if results := misc.get_nested_key(test, "misc/results", default=[]):
            status_priority = {status: -idx for idx, status in enumerate(KCIDB_STATUS_CHOICES)}
            status_priority[None] = -99
            max_result_status = None

            for result in results:
                if status := result.get("status"):
                    result["status"] = sanitize_kcidb_status(status)

                    if status_priority[result["status"]] > status_priority[max_result_status]:
                        max_result_status = result["status"]

            if test.get("status") != max_result_status:
                LOGGER.error("Mismatching Test.status and greatest TestResult.status (%r != %r)",
                             test.get("status"), max_result_status)
                if status_priority[test.get("status")] < status_priority[max_result_status]:
                    LOGGER.info("Setting Test.status to greatest TestResult.status")
                    test["status"] = max_result_status


def _validate_kcidb(data):
    """Validate kcidb with additional checks."""
    try:
        KCIDB_SCHEMA.validate(data)
    except jsonschema.exceptions.ValidationError as validation_error:
        raise ValidationError("Failed to validate against the KCIDB schema.",
                              validation_error) from None


def _validate_checkouts_origin(data):
    """Check if origin is in checkout id."""
    for checkout in data.get('checkouts', []):
        if not checkout['id'].startswith(checkout['origin'] + ':'):
            raise ValidationError(
                f"checkout id ({checkout['id']}) does not match "
                f"origin ({checkout['origin']}) constraint"
            )


def validate_kcidb(data):
    """Validate kcidb with additional checks.

    Deprecated, use `cki_lib.kcidb.validate_extended_kcidb_schema` instead.
    """
    warn(
        ("Function `cki_lib.kcidb.validate.validate_kcidb` will be deprecated soon,"
         " please use `cki_lib.kcidb.validate_extended_kcidb_schema`."),
        DeprecationWarning, stacklevel=2)
    _validate_kcidb(data)
    _validate_checkouts_origin(data)


def _validate_cki_schema(data):
    """Validate data against the CKI's extended KCIDB schema, which constraints the misc fields."""
    try:
        jsonschema.validate(
            instance=data,
            schema=SCHEMA,
            format_checker=jsonschema.Draft7Validator.FORMAT_CHECKER
        )
    except jsonschema.exceptions.ValidationError as validation_error:
        raise ValidationError("Failed to validate against the CKI's extended KCIDB schema.",
                              validation_error) from None


def validate_extended_kcidb_schema(data, raise_for_cki=True):
    """Validate data against the KCIDB schema with CKI's extension."""
    sanitize_all_kcidb_status(data)

    _validate_kcidb(data)
    _validate_checkouts_origin(data)
    try:
        _validate_cki_schema(data)
    except ValidationError:
        # Temporary try-except to avoid crashing dependents failing to validate against the schema.
        # This can be removed once nothing is using validate()
        if raise_for_cki:
            raise
        LOGGER.exception("Failure to validate against the schema may crash in the future.")


def validate(data):
    """Validate data against the KCIDB schema with CKI's extension.

    Deprecated, use `cki_lib.kcidb.validate_extended_kcidb_schema` instead.
    """
    warn(
        ("Function `cki_lib.kcidb.validate.validate` will be deprecated soon,"
         " please use `cki_lib.kcidb.validate_extended_kcidb_schema`."),
        DeprecationWarning, stacklevel=2)

    validate_extended_kcidb_schema(data, raise_for_cki=False)


def main(argv=None):
    """Run the command line interface."""
    parser = argparse.ArgumentParser()
    parser.add_argument('file')

    args = parser.parse_args(argv)

    validate_extended_kcidb_schema(json.loads(pathlib.Path(args.file).read_text(encoding='utf8')))


if __name__ == "__main__":
    main()
