"""Cluster management for Kubernetes-based integration tests."""
import contextlib
import datetime
import json
import os
import subprocess
import time
import typing
from urllib import parse

from kubernetes import client
from kubernetes import config
from kubernetes import dynamic
from kubernetes import watch

from . import test
from .. import logger
from .. import misc

LOGGER = logger.get_logger(__name__)


class KubernetesCluster(test.TestCaseWithContext):
    """Kubernetes-based integration test."""

    cluster_name = 'cki'
    known_ports = (
        ('rabbitmq',  5671, 30000),  # AMQP via TLS
        ('rabbitmq',  5672, 30001),  # AMQP
        ('rabbitmq', 15671, 30002),  # HTTP API via TLS
        ('rabbitmq', 15672, 30003),  # HTTP API
        ('rabbitmq', 61613, 30004),  # Stomp
        ('rabbitmq', 61614, 30005),  # Stomp via TLS
    )

    hostname: str = ''
    api_client: client.ApiClient = None
    dynamic_client: dynamic.client.DynamicClient = None
    core_v1: client.CoreV1Api = None

    @classmethod
    def setUpClass(cls) -> None:
        """Provision the cluster."""
        super().setUpClass()
        cls.enterClassContext(cls._cluster())

    @staticmethod
    def _is_ci() -> bool:
        return 'DOCKER_HOST' in os.environ

    @classmethod
    @contextlib.contextmanager
    def _cluster(cls) -> typing.Iterator[None]:
        if cls._is_ci():
            cls.hostname = typing.cast(str, parse.urlparse(os.environ['DOCKER_HOST']).hostname)
        else:
            cls.hostname = 'localhost'

        clusters = subprocess.run(['kind', 'get', 'clusters'],
                                  capture_output=True, encoding='utf8', check=True).stdout
        if cls.cluster_name not in clusters.strip().split('\n'):
            LOGGER.info('Creating cluster "%s"', cls.cluster_name)
            kind_config: dict[str, typing.Any] = {
                'apiVersion': 'kind.x-k8s.io/v1alpha4', 'kind': 'Cluster',
                'name': cls.cluster_name,
                'nodes': [{
                    'role': 'control-plane',
                    'extraPortMappings': list(
                        {'containerPort': n, 'hostPort': c} for _, c, n in cls.known_ports
                    ),
                }],
            }
            if cls._is_ci():
                # add to the apiServer certSANs the name of the docker (dind)
                # service in order to be able to reach the cluster through it
                cert_patch = [
                    {'op': 'add', 'path': '/apiServer/certSANs/-', 'value': cls.hostname}]
                kind_config.update({
                    'networking': {'apiServerAddress': '0.0.0.0', 'apiServerPort': 6443},
                    'kubeadmConfigPatchesJSON6902': [{
                        'group': 'kubeadm.k8s.io',
                        'version': 'v1beta3',
                        'kind': 'ClusterConfiguration',
                        'patch': json.dumps(cert_patch),
                    }],
                })
            subprocess.run(['kind', 'create', 'cluster', '--config', '-'],
                           input=json.dumps(kind_config), encoding='utf8', check=True)

        client_config = client.Configuration()
        config.load_kube_config(context=f'kind-{cls.cluster_name}',
                                client_configuration=client_config)
        if cls._is_ci():
            server_url = parse.urlparse(client_config.host)
            server_url = server_url._replace(netloc=f'{cls.hostname}:{server_url.port}')
            client_config.host = server_url.geturl()
        cls.api_client = client.ApiClient(configuration=client_config)
        cls.dynamic_client = dynamic.client.DynamicClient(cls.api_client)
        cls.core_v1 = client.CoreV1Api(cls.api_client)

        yield

    @classmethod
    @contextlib.contextmanager
    def k8s_namespace(cls, namespace: str) -> typing.Iterator[None]:
        """Create and clean up a k8s namespace."""
        try:
            cls.core_v1.read_namespace(namespace)
        except client.ApiException:
            LOGGER.info('Creating namespace "%s"', namespace)
            cls.core_v1.create_namespace(client.V1Namespace(
                metadata=client.V1ObjectMeta(name=namespace)))

        yield

        if os.environ.get('CKI_INTEGRATION_TESTS_CLEANUP') == 'skip':
            LOGGER.warning('Skipping cleanup of resources in "%s"', namespace)
        else:
            LOGGER.info('Deleting resources in "%s"', namespace)
            cls.k8s_delete_all(namespace, ('Deployment', 'ConfigMap', 'Secret', 'Service'))

    @classmethod
    def k8s_apply(cls, namespace: str, body: typing.Any) -> None:
        """Server-side apply a resource."""
        cls.dynamic_client.resources.get(kind=body['kind']).server_side_apply(
            namespace=namespace, field_manager='cki-integration-tests', body=body)

    @classmethod
    def k8s_wait(cls, namespace: str, name: str, setup_at: datetime.datetime) -> bool:
        """Wait for a deployment to become available."""
        if not cls._wait(namespace, 'Pod', 'Ready', [
            ('object|metadata|labels|app', name),
            ('object|metadata|annotations|cki-project.org/setup-at', setup_at.isoformat()),
        ]):
            return False
        if not cls._wait(namespace, 'Deployment', 'Available', [
            ('object|metadata|name', name),
        ]):
            return False
        time.sleep(1)  # give the Service magic a bit more time to settle to avoid port timeouts
        LOGGER.debug('Waited %s for %s to become ready', misc.now_tz_utc() - setup_at, name)
        return True

    @classmethod
    def k8s_delete_all(cls, namespace: str, resources: typing.Iterable[str]) -> None:
        """Remove all resources in a certain namespace."""
        for kind in resources:
            resource = cls.dynamic_client.resources.get(kind=kind)
            for item in resource.get(namespace=namespace)['items']:
                resource.delete(namespace=namespace, name=item.metadata.name)

    @classmethod
    def _wait(cls, namespace: str, kind: str, condition: str,
              checks: list[tuple[str, str]]) -> bool:
        watcher = watch.Watch()
        LOGGER.debug('Waiting for %s', kind)
        for event in cls.dynamic_client.resources.get(kind=kind).watch(
                timeout=60, watcher=watcher, namespace=namespace):
            if not all(misc.get_nested_key(event, k, lookup_attrs=True, delimiter='|') == v
                       for k, v in checks):
                continue
            if any(c['type'] == condition and misc.strtobool(c['status'])
                   for c in (event['object'].status.conditions or [])):
                watcher.stop()
                return True
            # event.type: ADDED, MODIFIED, DELETED
            if event['type'] == 'DELETED':
                LOGGER.debug('deleted before it started')
                watcher.stop()
                return False
        LOGGER.debug('timeout')
        return False
