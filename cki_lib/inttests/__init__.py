"""Kubernetes-based integration tests."""
import shutil
import typing
import unittest

from . import cluster
from . import rabbitmq
from ..logger import get_logger

LOGGER = get_logger(__name__)

# before review:
# - more docs about (ci) config


__all__ = ['skip_without_requirements', 'KubernetesIntegrationTest', 'cluster', 'rabbitmq']


def skip_without_requirements() -> typing.Callable[[typing.Any], typing.Any]:
    """Skip without the required dependencies."""
    for dependency in ('kind', 'docker'):
        if shutil.which(dependency) is None:
            return unittest.skip(f'Required "{dependency}" was not found in PATH')
    return lambda func: func


class KubernetesIntegrationTest(cluster.KubernetesCluster):
    """Kubernetes-based integration test."""

    test_namespace = 'integration-test'

    def setUp(self) -> None:
        """Provision the namespace."""
        super().setUp()
        self.enterContext(self.k8s_namespace(self.test_namespace))
