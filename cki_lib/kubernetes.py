"""Kubernetes Helper."""
import os
import pathlib

import kubernetes


class KubernetesHelper:
    """Provide some Kubernetes helper functions."""

    cluster_namespace_path = pathlib.Path('/var/run/secrets/kubernetes.io/serviceaccount/namespace')

    def __init__(self):
        """Create a new Kubernetes helper."""
        self.config = kubernetes.client.Configuration()
        self.config_type = None
        self.namespace = None

    def setup(self):
        """Set up the configuration to allow API calls.

        The configuration will be determined from (in this order):
        - the current context from ~/.kube/config if this file exists
        - /var/run/secrets/kubernetes.io/serviceaccount inside the cluster
        """
        if self.cluster_namespace_path.exists():
            self._setup_cluster()
        else:
            self._setup_kube_config()

    @property
    def api_client(self):
        """Return ApiClient."""
        return kubernetes.client.ApiClient(configuration=self.config)

    def api_corev1(self):
        """Return a Kubernetes CoreV1Api client."""
        return kubernetes.client.CoreV1Api(self.api_client)

    def _setup_cluster(self):
        self.config_type = 'cluster configuration'
        kubernetes.config.load_incluster_config(self.config)
        self.namespace = self.cluster_namespace_path.read_text(encoding='utf8')

    def _setup_kube_config(self):
        self.config_type = '~/.kube/config'
        kubernetes.config.load_kube_config(client_configuration=self.config)
        config_contexts = kubernetes.config.list_kube_config_contexts()
        self.config.ssl_ca_cert = os.environ['REQUESTS_CA_BUNDLE']
        self.namespace = config_contexts[1]['context']['namespace']
