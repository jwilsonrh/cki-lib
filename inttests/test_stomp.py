"""Tests for Stomp message sending."""
import contextlib
from unittest import mock

from cki_lib import inttests
from cki_lib import messagequeue
from cki_lib import stomp


@inttests.skip_without_requirements()
class TestStompClient(inttests.KubernetesIntegrationTest,
                      inttests.rabbitmq.RabbitMQServer):
    """Tests for StompClient."""

    def setUp(self) -> None:
        """Declare MessageQueue instance and exchanges."""

        self.messagequeue = messagequeue.MessageQueue(dlx_retry=False)

        self.queues: list[str] = []
        self.exchanges = ['exchange.one']

        with self.messagequeue.connect() as channel:
            for exchange in self.exchanges:
                channel.exchange_declare(exchange)

    def tearDown(self) -> None:
        """Delete all queues and exchanges from the server."""
        with self.messagequeue.connect() as channel:
            for queue in self.queues:
                with contextlib.suppress(Exception):
                    channel.queue_delete(queue)
            for exchange in self.exchanges:
                with contextlib.suppress(Exception):
                    channel.exchange_delete(exchange)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_stomp_send_message(self) -> None:
        """Test publishing and receiving a message."""
        callback = mock.Mock()
        exchange = 'exchange.one'
        queue = 'queue.one'
        # append for deletion on teardown
        self.queues.append(queue)

        # first consume to create the queue.
        self.messagequeue.consume_messages(
            exchange, ['#'], callback,
            queue_name=queue, inactivity_timeout=0.1)
        self.assertEqual(0, callback.call_count)

        stomp.StompClient().send_message({'foo': 'bar'}, 'queue.one')

        self.messagequeue.consume_messages(
            exchange, ['#'], callback,
            queue_name=queue, inactivity_timeout=1)
        self.assertEqual(1, callback.call_count)
        self.assertEqual(callback.mock_calls[0].kwargs['body'], {'foo': 'bar'})
