"""Test MessageQueue."""
from contextlib import suppress
import os
import time
from unittest import mock

from cki_lib import inttests
from cki_lib import messagequeue
from cki_lib import misc
from cki_lib import session

SESSION = session.get_session(__name__)


class CallbackMock:
    """Fake callback."""

    def __init__(self, fail_on_call=None):
        """
        Initialize.

        fail_on_call: list of call number where the callback should fail.
        """
        self.messages = []
        self.fail_on_call = fail_on_call or []
        self.call_count = 0

    def __call__(self, routing_key=None, body=None, **_):
        """
        Callback call.

        If the call count is on fail_on_call, it will raise Exception.
        Otherwise, it will store the (routing_key, body) in messages.
        """
        if self.call_count in self.fail_on_call:
            self.call_count += 1
            raise Exception()

        self.call_count += 1
        self.messages.append(
            (routing_key, body)
        )


@inttests.skip_without_requirements()
class IntegrationTestMessageQueue(inttests.KubernetesIntegrationTest,
                                  inttests.rabbitmq.RabbitMQServer):
    # pylint: disable=too-many-public-methods
    """Run integration tests for cki_lib/messagequeue.py."""

    def setUp(self):
        """Declare MessageQueue instance and exchanges."""
        host = os.environ['RABBITMQ_HOST']
        port = misc.get_env_int('RABBITMQ_PORT', 5672)
        management_port = misc.get_env_int('RABBITMQ_MANAGEMENT_PORT', 15672)
        user = os.environ.get('RABBITMQ_USER', 'guest')
        password = os.environ.get('RABBITMQ_PASSWORD', 'guest')

        self.messagequeue = messagequeue.MessageQueue(host=host, port=port,
                                                      user=user, password=password)

        self.queues = []
        self.exchanges = [
            'exchange.one',
            'cki.exchange.retry.incoming',
            'cki.exchange.retry.outgoing',
        ]

        with self.messagequeue.connect() as channel:
            for exchange in self.exchanges:
                channel.exchange_declare(exchange)

        self._management_if = f'https://{host}:{management_port}'
        self._management_auth = (user, password)
        SESSION.put(f'{self._management_if}/api/policies/%2F/tests-ttl',
                    verify=os.environ['RABBITMQ_CAFILE'],
                    auth=self._management_auth,
                    json={
                        'vhost': '/',
                        'name': 'tests-ttl',
                        'pattern': r'cki\.queue\.retry\..*',
                                   'apply-to': 'queues',
                                   'definition': {'message-ttl': 1000},
                    }).raise_for_status()

    def tearDown(self):
        """Delete all queues and exchanges from the server."""
        with self.messagequeue.connect() as channel:
            for queue in self.queues:
                with suppress(Exception):
                    channel.queue_delete(queue)
            for exchange in self.exchanges:
                with suppress(Exception):
                    channel.exchange_delete(exchange)
        SESSION.delete(f'{self._management_if}/api/policies/%2F/tests-ttl',
                       verify=os.environ['RABBITMQ_CAFILE'],
                       auth=self._management_auth,
                       json={
                           'vhost': '/',
                           'name': 'tests-ttl',
                           'component': 'policy',
                       }).raise_for_status()

    @mock.patch('cki_lib.messagequeue.misc.is_production', mock.Mock(return_value=True))
    def test_basic_consume(self):
        """Test publishing and receiving a message."""
        callback = CallbackMock()
        exchange = 'exchange.one'
        queue = 'queue.one'
        # Append for deletion on teardown
        self.queues.extend([
            queue,
            f'cki.queue.retry.{queue}'
        ])

        # First consume to create the queue.
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, inactivity_timeout=0.1)
        self.assertEqual(0, callback.call_count)

        self.messagequeue.send_message({'foo': 'bar'}, '#', exchange=exchange)
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, inactivity_timeout=0.1)

        self.assertEqual(1, callback.call_count)
        self.assertEqual(
            callback.messages,
            [('#', {'foo': 'bar'})]
        )

    @mock.patch('cki_lib.messagequeue.misc.is_production', mock.Mock(return_value=True))
    def test_basic_consume_anonymous_queue_prod_fails(self):
        """Test that anonymous queues are not allowed in production mode."""
        self.assertRaises(Exception, self.messagequeue.consume_messages,
                          'exchange.one', ['#'], CallbackMock(), inactivity_timeout=0.1)

    @mock.patch('cki_lib.messagequeue.misc.is_production', mock.Mock(return_value=True))
    def test_basic_dlx_requeue(self):
        """Test that a message is requeued after nacking it."""
        callback = CallbackMock(fail_on_call=[0])
        exchange = 'exchange.one'
        queue = 'queue.one'
        # Append for deletion on teardown
        self.queues.extend([
            queue,
            f'cki.queue.retry.{queue}'
        ])

        # First consume to create the queue.
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, inactivity_timeout=0.01)

        # Send a message
        self.messagequeue.send_message({'foo': 'bar'}, '#', exchange=exchange)

        # First consume will raise Exception and requeue.
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, inactivity_timeout=0.01)
        self.assertEqual(1, callback.call_count)
        self.assertEqual(0, len(callback.messages))

        # Check after a short period of time to ensure it's not requeued instantly
        time.sleep(0.5)
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, inactivity_timeout=0.01)
        self.assertEqual(1, callback.call_count)
        self.assertEqual(0, len(callback.messages))

        # Wait until the message gets requeued
        time.sleep(1)

        # New consume will get the message again and not requeue.
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, inactivity_timeout=0.01)
        self.assertEqual(2, callback.call_count)
        self.assertEqual(1, len(callback.messages))

        # Wait to check that the message does not get requeued
        time.sleep(1.1)

        # New consume should not get new messages.
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, inactivity_timeout=0.01)
        self.assertEqual(2, callback.call_count)

        # Test message routing_key is correctly restored.
        self.assertListEqual(
            [('#', {'foo': 'bar'})],
            callback.messages,
        )

    @mock.patch('cki_lib.messagequeue.misc.is_production', mock.Mock(return_value=True))
    def test_basic_dlx_requeue_same_consume(self):
        """
        Test that a message is requeued after nacking it.

        On a single connection, it should get the message more than once.
        """
        callback = CallbackMock(fail_on_call=[0])
        exchange = 'exchange.one'
        queue = 'queue.one'
        # Append for deletion on teardown
        self.queues.extend([
            queue,
            f'cki.queue.retry.{queue}'
        ])

        # First consume to create the queue.
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, inactivity_timeout=0.01)

        # Send a message
        self.messagequeue.send_message({'foo': 'bar'}, '#', exchange=exchange)

        # Consume for a long time to catch the first message and the requeued one.
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, inactivity_timeout=2)
        self.assertEqual(2, callback.call_count)
        self.assertEqual(1, len(callback.messages))

    @mock.patch('cki_lib.messagequeue.misc.is_production', mock.Mock(return_value=True))
    def test_basic_dlx_requeue_multiple_queues(self):
        """
        Test that a message is requeued after nacking it.

        Check with multiple queues that the message is only routed into the correct one.
        """
        callback = CallbackMock(fail_on_call=[0])
        exchange = 'exchange.one'
        queue_one = 'queue.one'
        queue_two = 'queue.two'
        # Append for deletion on teardown
        self.queues.extend([
            queue_one,
            queue_two,
            f'cki.queue.retry.{queue_one}'
            f'cki.queue.retry.{queue_two}'
        ])

        # First consume to create the queues
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue_one, inactivity_timeout=0.01)
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue_two, inactivity_timeout=0.01)

        # Send a message
        self.messagequeue.send_message({'foo': 'bar'}, '#', exchange=exchange)

        # Consume on queue_one fail. Requeued
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue_one, inactivity_timeout=0.01)
        self.assertEqual(1, callback.call_count)

        # Consume on queue_two ok
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue_two, inactivity_timeout=0.01)
        self.assertEqual(2, callback.call_count)

        time.sleep(1)

        # Consume on queue_one has a new message after it was requeued
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue_one, inactivity_timeout=0.01)
        self.assertEqual(3, callback.call_count)

        # Consume on queue_two has no new messages (not requeued)
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue_two, inactivity_timeout=0.01)
        self.assertEqual(3, callback.call_count)

    @mock.patch('cki_lib.messagequeue.misc.is_production', mock.Mock(return_value=True))
    def test_basic_consume_priority(self):
        """Test publishing and receiving priorized messages."""
        callback = CallbackMock()
        exchange = 'exchange.one'
        queue = 'queue.one'
        # Append for deletion on teardown
        self.queues.extend([
            queue,
            f'cki.queue.retry.{queue}'
        ])

        # First consume to create the queue.
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, max_priority=1, inactivity_timeout=0.1)
        self.assertEqual(0, callback.call_count)

        self.messagequeue.send_message({'priority': '0'}, '#', exchange=exchange)
        self.messagequeue.send_message({'priority': '1'}, '#', exchange=exchange, priority=1)
        self.messagequeue.consume_messages(
            exchange, ['#'], callback, queue_name=queue, max_priority=1, inactivity_timeout=0.1)

        self.assertEqual(2, callback.call_count)
        self.assertEqual(
            callback.messages,
            [
                ('#', {'priority': '1'}),
                ('#', {'priority': '0'}),
            ]
        )
