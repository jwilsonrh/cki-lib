# cki-lib

Common place to put scripts used across CKI infrastructure.

## Design principles

This repository holds common code that is used by at least two other CKI
Project applications.

## Detailed documentation of Python tools

| Tool               | Description                                               |
|--------------------|-----------------------------------------------------------|
| `cki_lib.footer`   | [documentation](documentation/README.cki_lib.footer.md)   |
| `cki_lib.gitlab`   | [documentation](documentation/README.cki_lib.gitlab.md)   |
| `cki_lib.inttests` | [documentation](documentation/README.cki_lib.inttests.md) |

## Detailed documentation of shell scripts

| Tool                 | Description                                              |
|----------------------|----------------------------------------------------------|
| `cki_check_links.sh` | [documentation](documentation/README.cki_check_links.md) |
| `cki_entrypoint.sh`  | [documentation](documentation/README.cki_entrypoint.md)  |
| `cki_lint.sh`        | [documentation](documentation/README.cki_lint.md)        |

## Developer guidelines

**Use care when making changes to this repository.** Many other applications
depend on this code to work properly.

### Setup

If you want to get started with development, you can install the
dependencies by running:

```shell
pip install --user .[dev]
sudo dnf install shellcheck
```

### Running tests and linting

See [documentation/README.cki_lint.sh](documentation/README.cki_lint.sh) on how
to run tests and linting.
